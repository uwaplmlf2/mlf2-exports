
/************************************************************/
/* MIZ    Timer/Command/Error Exception handling functions	*/

/*	Each function returns								*/
/*	0 if Ballast.c can proceed normally					*/
/*	-1 if immediate return is needed					*/
/*	1 if the current mode needs to terminate gracefully	*/
/*		(mode is set to new_mode, and next_mode is called prior to return)	*/
/************************************************************/

/* This function will handle timers */
int handle_timer(int mode, double day_time, int timer_command, int *mode_out)
{ // We can switch modes & stages based on the timer_command.
    // Old functionality (stage switching) can be replicated by setting stage = timer_command and returning 1 to end the mode.
    // The only problem for now is signalling stage reset (but mode_out should help with that)
    int result = 0;
    if (timer_command == 1)
    { // time to surface!
        Ice.act_now = 1; // will make the decision based on the next altimeter run!
        Ice.day_time_last = -1; // Force the altimeter run
        log_event("Setting 'act now' flag.\n");
    }
    else
    {
        log_event("Bad timer command (%d)\n", timer_command);
    }
    return result;
}

/* This function will handle commands */
int handle_command(int mode, double day_time, int command, int *mode_out)
{
    int surfcheck_result;
    // SURFCHECK command, process the new result
#ifndef SIMULATION
    surfcheck_result = get_surfcheck_result();
#else
    surfcheck_result = command; // in simulation, keep passing via command (so that the simulator could trigger it)
#endif
    return check_surfacing_rules(day_time, surfcheck_result); // will return 1 if need to terminate current mode
}