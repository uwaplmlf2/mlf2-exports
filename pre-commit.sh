#!/bin/bash
# Pre-commit hook to ensure that the code compiles without
# errors before it is commited. This requires the "vc" script
# to compile the code in a Virtual Machine.
#
export PATH=$HOME/bin:$PATH

# Don't bother running if 'vc' is not available
which vc > /dev/null || exit 0

# Specify the mission type
MTYPE=spurs2

# Stash any unsaved changes
git stash -q --keep-index
# Try to build the software
vc scons mtype=$MTYPE
status=$?
# Restore the stash
git stash pop -q
exit $status
